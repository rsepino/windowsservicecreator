# WindowsServiceCreator

This will create a service that can be used to serve as a wrapper to your application.

## Requirements
1. Your application (.exe file).


## How to setup/Initial setup
1. Clone the repository and copy the files to where your application is.
2. Using a `cmd` with `Admin` rights, run `installService.bat`, this will create `%USERPROFILE%\settings.json` file.
3. Edit the `%USERPROFILE%\settings.json` to your desired values. See **Configuration** section for the meaning of the values.

<pre>
{
	"ServiceName": "SelfService",
	"Description": "This is the description from settings.",
	"Username": ".\\",
	"Password": "",
	"OnStart": {
		"exe":"",
		"args":""
	},
	"OnAwake": {
		"exe":"",
		"args":""
	},
	"OnStop": {
		"exe":"",
		"args":""
	}
}
</pre>

4. Run `installService.bat` with the new `ServiceName` you provided. (`installService.bat <NewServiceName>`)

## Uninstallation

1. Run `uninstallService.bat` with the `ServiceName` inside `%USERPROFILE%\\settings.json`. (`uninstallService.bat <NewServiceName>`)

## Configuration

**Notes:**

1. Be careful when using the escape character `\`. **.\myUserAccount** in simple text, `.\\myUserAccount` in JSON file.
2. For the `Username`, `.\\` is already provided because the `.` is for the domain, which in this case means your own pc/machine.
3. Notice the escape character `\` on the `args` when using double quotes `"`

| Key | Description | Sample values |
| ------ | ------ | ------ | 
| ServiceName | The desired name of the service | SelfService |
| Description | Description of the service. | This is the description from settings. |
| Username | The user account that will be used to install the service. | `.\\myUserAccount` |
| Password | The password of the user account. | myuserAccounTPwd |
| OnStart.exe | The executable file that will be run when the service starts. | "C:\\<path to your exe>\\yourApp.exe" |
| OnStart.args | The arguments needed to run the `OnStart.exe`. | "--arg \\"string argument\\"" |
| OnAwake.exe | The application you want to run when the service awakes after `5 seconds`. | "C:\\Windows\\System32\\cmd.exe" |
| OnAwake.args | The arguments needed to run the `OnAwake.exe`. | "/c echo OnAwake>>%USERPROFILE%\\Desktop\\Service_OnAwake.txt" |
| OnStop.exe | Make sure to kill your application on this stage. | "C:\\Windows\\System32\\cmd.exe" |
| OnStop.args | The arguments needed to kill your application. | "/F /IM yourApp.exe" |

## Checking your Setup

### Task manager
1. Using `task manager`, go to `Services` and look for your service. 
2. Check your service if it has `PID` and is running.
3. Check also in the `Details` tab of the task manager if `yourApp.exe` is running.
4. If not, then there is something wrong with your setup